/* Jellyfish Client
 * Version 1.1
 */

#include <stdio.h>

__declspec(dllimport) int add(int a, int b);
__declspec(dllimport) double mult(double a, double b);
__declspec(dllimport) void cool();
__declspec(dllimport) void dialogAbout();
__declspec(dllimport) void beeper();
int main(int ac, char *av[]) {
	dialogAbout();
	cool();
	printf("%s: Hello, %d!\n", av[0], add(5, 5));
	printf("5.5 * 5.5 = %g!\n", mult(5.5, 5.5));
	beeper();
	printf("Now exiting!\n");
	return 0;
}

