/* Jelly DLL
 * Version 1.1
 */

#include <stdio.h>
#include <Windows.h>

int add(int a, int b) {
	printf("Adding %d and %d.\n", a, b);
	return a+b;
}

double mult(double a, double b) {
	printf("Multiplying %g and %g.\n", a, b);
	return a*b;
}

void cool() {
	int x;
	for(x = 1; x <= 15; x++) {
		printf("Cool number %d is here!\n", x);
	}
	printf("Done this time!\nPrinted from DLL.\n");
}

void dialogAbout() {
	HMODULE m = GetModuleHandleW(NULL);
	HICON i = LoadIconW(m, MAKEINTRESOURCEW(101));
	ShellAboutW(NULL, L"Jellyfish Client#Jellyfish Client DLL", L"This is the Jellyfish DLL Client, an example of how to write a DLL and use it in your program.", i);
	Beep(1000, 500);
}

void beeper() {
	Beep(750, 500);
}

