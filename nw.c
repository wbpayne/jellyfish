#include <stdio.h>
#include <windows.h>
#pragma warning( disable : 4312 )
int main() {
	HMODULE jelly;
	LONG ordcall;
	jelly = LoadLibrary("jelly.dll");
	ordcall = MAKELONG(4,0);
	void (*ver)() = (void (__cdecl *)())GetProcAddress(jelly, (LPCSTR)ordcall);
	ordcall = MAKELONG(5,0);
	void (*beep)() = (void (__cdecl *)())GetProcAddress(jelly, (LPCSTR)ordcall);
	ver();
	beep();
	return 0;
}

